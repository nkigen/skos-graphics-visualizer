package graphics.help;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;

public class About extends JInternalFrame {
	
	
	public About()
	{
		super("About ThVisual");
		setSize(400, 350);
		displayGraphics();
	}
	private void displayGraphics()
	{
		setClosable(true);
		setIconifiable(true); 
		setBackground(Color.white);
		setMaximizable(false);
		FlowLayout layout = new FlowLayout();
		
		layout.setAlignOnBaseline(true);
		setLayout(layout);
		
		add(new JLabel("<html><b>ThViewer</b> </html>"));
		
		
		setVisible(true);
	}
}
