package graphics.main.eventlisteners;

import graphics.help.About;
import graphics.main.DisplayVisualizer;
import graphics.thesaurus.Concept;
import graphics.thesaurus.ConceptDetails;
import graphics.thesaurus.History;
import graphics.thesaurus.Search;
import graphics.thesaurus.Summary;
import graphics.thesaurus.visualizer.Visualizer;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import org.semanticweb.skos.SKOSDataset;
import org.semanticweb.skosapibinding.SKOSManager;

import load.LoadThesaurus;

public class MenuEvent implements ActionListener {
	
	private JMenuItem item;
	private JDesktopPane desktop;
	private DisplayVisualizer frame;
	private Map<URI, Concept> thesauri;
	
	public MenuEvent(JMenuItem item,JDesktopPane desktop)
	{
		this.item=item;
		this.desktop=desktop;
		this.item.addActionListener(this);
	}
	public MenuEvent(JMenuItem item,DisplayVisualizer frame)
	{
		this.item=item;
		this.frame=frame;
		this.item.addActionListener(this);
	}
	public MenuEvent(JMenuItem item,JDesktopPane desktop, DisplayVisualizer visual )
	{
		this.item=item;
		this.desktop=desktop;
		this.item.addActionListener(this);
		this.frame=visual;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(item.getText().equals("Search"))
		{
			String find=JOptionPane.showInputDialog(null,"Enter the text to search");
			
			Search search=new Search(find, frame.getThesauri());
			
			if(search.getResult()!=null)
			{
				desktop.add(new ConceptDetails(search.getResult()));
			}
			else
			{
				JOptionPane.showMessageDialog(null, "The phrase could not be found!");
			}
			
		}
		
		else if(item.getText().equals("Load"))
		{
		   String uri=JOptionPane.showInputDialog(null,"Enter the URI for the Thesaurus");
		   System.out.println(uri);
		   
		   LoadThesaurus load=new LoadThesaurus(uri);
		   try {
			if(load.getResponseCode(uri)==HttpURLConnection.HTTP_OK)
			   {
				
				load.load();
				SKOSManager manager = load.getManager();
				SKOSDataset dataset=load.getDataset();
				frame.setThesauri(load.getVocabulary());
				desktop.add(new Visualizer(frame.getThesauri(),desktop));
				   
			   }
			else
			{
				JOptionPane.showMessageDialog(null, "Invalid URI!");
			}
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Invalid URI!");
			//e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
		}
			
		}
		else if(item.getText().equals("History"))
		{
			desktop.add(new History());
		}
		else if(item.getText().equals("2D"))
		{
			desktop.add(new Visualizer(frame.getThesauri(),desktop));
		}
		else if(item.getText().equals("About"))
		{
			desktop.add(new About());
		}
		else if(item.getText().equals("Exit"))
		{
			WindowEvent wev= new WindowEvent(frame, WindowEvent.WINDOW_CLOSING);
			Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
		}
		else if(item.getText().equals("Summary"))
		{
			desktop.add(new Summary());
		}
	}
	
	public Map<URI,Concept> getThesaurus()
	{
		return thesauri;
		
	}

}
