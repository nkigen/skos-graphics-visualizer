package graphics.main;

import graphics.main.eventlisteners.MenuEvent;
import graphics.thesaurus.Concept;
import graphics.thesaurus.ConceptDetails;
import graphics.thesaurus.History;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.net.URI;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
public class DisplayVisualizer extends JFrame {

  private Map<URI,Concept> thesauri;

	public DisplayVisualizer()
	{
		super("ThVisual");
		Dimension screensize=Toolkit.getDefaultToolkit().getScreenSize();
		
		setBounds(0,0,screensize.width, screensize.height);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		
		//menubar 
		
		JMenuBar menubar=new JMenuBar();
		
		menubar.setVisible(true);
		setJMenuBar(menubar);	
		JMenu filemenu=new JMenu("File");
		JMenu editmenu=new JMenu("Edit");
		JMenu viewmenu=new JMenu("View");
		JMenu extramenu=new JMenu("Extras");
		JMenu helpmenu=new JMenu("Help");
		//add items to menubar
		
		JMenuItem exititem=new JMenuItem("Exit");
		exititem.setToolTipText("Exit ThVisual");
		JMenuItem loaditem=new JMenuItem("Load");
		loaditem.setToolTipText("Load a new SKOS Thesaurus");
		
		
		JMenuItem historyitem=new JMenuItem("History");
         historyitem.setToolTipText("View all your history");
         
         JMenuItem summaryitem=new JMenuItem("Summary");
         historyitem.setToolTipText("A summary of the currently loaded thesaurus");
         
         
         JMenuItem searchitem=new JMenuItem("Search");
         searchitem.setToolTipText("Search for a word ");
         
         
         JMenuItem visualitem=new JMenuItem("2D");
         
         
         JMenuItem helpitem=new JMenuItem("Help (F1)");
         JMenuItem aboutitem=new JMenuItem("About");
         
         
		
		filemenu.add(loaditem);
		filemenu.add(exititem);
		
		viewmenu.add(historyitem);
		viewmenu.add(visualitem);
		viewmenu.add(summaryitem);
		
		editmenu.add(searchitem);
		
		helpmenu.add(helpitem);
		helpmenu.add(aboutitem);
		
		menubar.add(filemenu);
		menubar.add(editmenu);
		menubar.add(viewmenu);
		menubar.add(extramenu);
		menubar.add(helpmenu);
		
		filemenu.setVisible(true);
		
		JDesktopPane desktop = new JDesktopPane();
		
		searchitem.addActionListener(new MenuEvent(searchitem,desktop,this));
		historyitem.addActionListener(new MenuEvent(historyitem, desktop) );
		loaditem.addActionListener(new MenuEvent(loaditem, desktop,this));
		visualitem.addActionListener(new MenuEvent(visualitem, desktop,this) );
		aboutitem.addActionListener(new MenuEvent(aboutitem, desktop) );
		summaryitem.addActionListener(new MenuEvent(summaryitem, desktop) );
		exititem.addActionListener(new MenuEvent(exititem, this) );
		
		setContentPane(desktop);
		
		setVisible(true);
	}
	
	public void setThesauri(Map<URI,Concept> thesauri)
	{
		this.thesauri=thesauri;
	}
	
	public Map<URI, Concept> getThesauri()
	{
		return thesauri;
	}

}
