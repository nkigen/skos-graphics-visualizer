package graphics.thesaurus;

import java.awt.FlowLayout;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.semanticweb.skos.SKOSEntity;


public class Concept{
	
	private Map<String,String> altLabel;
	private Map<String,String> prefLabel;
	private Map<String,String> scopeNote;
	private ArrayList<URI> narrower;
	private ArrayList<URI> broader;
	private URI inScheme;
	private ArrayList<URI> related;
	private URI uri;
	public Concept(URI uri)
	{
		this.uri=uri;
		altLabel=new HashMap<String,String>();
		prefLabel=new HashMap<String,String>();
		scopeNote=new HashMap<String,String>();
		narrower=new ArrayList<URI>();
		broader=new ArrayList<URI>();
		inScheme=null;
		related=new ArrayList<URI>();
		
		
	}
	
	public void setAltLabel(String altLabel,String lang)
	{
	 this.altLabel.put(altLabel, lang);
	}
	public void setPrefLabel(String prefLabel,String lang)
	{
		this.prefLabel.put(prefLabel,lang);
	}
	public void setScopeNote(String scopeNote,String lang)
	{
		this.scopeNote.put(scopeNote,lang);
	}
	
	public void setBroader(URI broader)
	{
		this.broader.add(broader);
	
	}
	public void setNarrower(URI narrower)
	{
		this.narrower.add(narrower);
	}
	public void setinScheme(URI inScheme)
	{
		this.inScheme=inScheme;
	}
	
	public void setRelated(URI related)
	{
		this.related.add(related);
	}
	
	public URI getUri()
	{
		return uri;
	}
	
	public ArrayList<URI> getRelated()
	{
		return related;
	}
	
	public ArrayList<URI> getBroader()
	{
		return broader;
	}
	
	public ArrayList<URI> getNarrower()
	{
		return narrower;
	}
	
	public Map<String, String> getPrefLabel()
	{
		return prefLabel;
	}
	
	public Map<String, String> getAltLabel()
	{
		return altLabel;
	}
	
	public URI getInscheme()
	{
		return inScheme;
	}

}
