package graphics.thesaurus;

import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import load.RecordHistory;

public class Search {
	
	
	private String find;
	
	private Concept result;

	
	public Search(String find,Map<URI, Concept> thesauri)
	{
		this.find=find;
		
		this.search(thesauri);
	}
	
  public boolean search(Map<URI, Concept> thesauri)
  {
	  Iterator<Entry<URI,Concept>> it= thesauri.entrySet().iterator();
	  System.out.println("Searching for :" + find);
	  System.out.println("size: "+thesauri.size());
	  while (it.hasNext())
	  {
		  System.out.println("Search ongoing...");
		  Entry pair=it.next();
		  
		  Concept concept=(Concept) pair.getValue();
		  
		  Iterator<Entry<String,String>> pref= concept.getPrefLabel().entrySet().iterator();
		  
		  while(pref.hasNext())
		  {
			  Entry prefpair=pref.next();
			  if(find.equalsIgnoreCase((String) prefpair.getKey()))
			  {
				  result=concept;
				  System.out.println("Parameter found !!!! :" + find);
				  return true;
			  }
			  
		  }
		  
		  Iterator<Entry<String,String>> alt= concept.getAltLabel().entrySet().iterator();
		  
		  while(alt.hasNext())
		  {
			  Entry altpair=alt.next();
			  if(find.equalsIgnoreCase((String) altpair.getKey()))
			  {
				  result=concept;
				  System.out.println("Parameter found !!!! :" + find);
				  new RecordHistory("history.dat", result.getUri().toString());
				  return true;
			  }
			  
		  }
		  
		  
		  
	  }
	  
	  System.out.println("NOT FOUND!! :" + find);
	  return false;
	  
  }
  
  public Concept getResult()
  {
	  return result;
  }

}
