package graphics.thesaurus;


import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.image.ImageProducer;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.semanticweb.skos.SKOSConcept;
import org.semanticweb.skos.SKOSDataset;

import graphics.thesaurus.visualizer.*;



public class ConceptDetails extends JInternalFrame{
	
	 Concept concept;
	public ConceptDetails(Concept concept)
	{
	super("concept");
	setSize(400, 350);
	this.concept=concept;
	displayGraphics();
	
	}
	public ConceptDetails()
	{
		super("Concept");
		setSize(400, 350);
		displayGraphics();
	}
	
	private void displayGraphics()
	{
		setClosable(true);
		setIconifiable(true); 
		setBackground(Color.white);
		setMaximizable(false);
		FlowLayout layout = new FlowLayout();
		//layout.setAlignment(FlowLayout.LEFT);
		layout.setAlignOnBaseline(true);
	 setLayout(layout);
	 String details = null;
	 Iterator<Entry<String,String>> it= concept.getPrefLabel().entrySet().iterator();
	 Iterator<Entry<String,String>> italt= concept.getAltLabel().entrySet().iterator();
	 
	 details="<html><br /><br />" +
	 		" URI :"+ concept.getUri().toString()+ "<br /><br />" +
	 			"InScheme :"+ concept.getInscheme().toString()  +	"<br /><br /><table><tr><td>PrefLabel</td><td></td></tr>";
			 while(it.hasNext())
			 {
				 Entry<String,String> pair= it.next();
				 details=details +"<tr><td>"+ pair.getKey() + "</td><td>@"+pair.getValue() + "</td></tr>";
			 }
			 details=details+"</table><br /><br /><table><tr><td>AltLabel</td><td></td></tr>";
			 
			 while(italt.hasNext())
			 {
				 Entry<String,String> pair= italt.next();
				 details=details +"<tr><td>"+ pair.getKey() + "</td><td>@"+pair.getValue() + "</td></tr>";
			 }
			 details=details +"</table></html>";
			 
			// ImagePanel panel = new ImagePanel(new ImageIcon("src/bg.jpg").getImage());
	
	// panel.add(new JLabel(details));
	 add(new JLabel(details));
	 setVisible(true);
	}

	

}
