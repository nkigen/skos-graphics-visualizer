package graphics.thesaurus;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class History extends JInternalFrame {
	
	
	
	public History()
	{
		super("History");
		setSize(400, 350);
		displayGraphics();
	}
	private void displayGraphics()
	{
		setClosable(true);
		setIconifiable(true); 
		setBackground(Color.white);
		setMaximizable(false);
		FlowLayout layout = new FlowLayout();
		
		layout.setAlignOnBaseline(true);
		setLayout(layout);
		
		JTextArea results=new JTextArea();
		
		results.setEditable(false);
		results.setAutoscrolls(true);
		results.setBackground(Color.WHITE);
		results.setSize(this.getWidth(),this.getHeight());
		
		add(results);
	 
	
	 setVisible(true);
	}



}
