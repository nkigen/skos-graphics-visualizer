package graphics.thesaurus.visualizer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.beans.PropertyVetoException;
import java.net.URI;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import graphics.thesaurus.Concept;
public class Visualizer extends JInternalFrame {
	
	JButton searchbutton;
	JTextField searchfield;
	JComboBox langList;
	String[] lang={"English","Spanish","Italian"};
	JPanel top,left,right,bottom,center;
	JTabbedPane leftpane;
	JComponent pane1,pane2;
	Map<URI,Concept> thesauri;
	
	public Visualizer(Map<URI,Concept> thesauri,JDesktopPane pane) {
		super("Visualizer");
		this.thesauri=thesauri;
		setSize(pane.getWidth(), pane.getHeight());
		displayGraphics();
	}
	private void displayGraphics()
	{
		setClosable(false);
		setIconifiable(true); 
		setBackground(Color.white);
		
		setMaximizable(true);
		BorderLayout layout = new BorderLayout();
		
		setLayout(layout);
		top=new JPanel(new FlowLayout());
		left=new JPanel(new GridLayout(1,1));
		right=new JPanel(new FlowLayout());
		center=new JPanel(new FlowLayout());
		bottom=new JPanel(new FlowLayout());
		
		layout.setHgap(5);
		layout.setVgap(5);
		bottom.setBackground(Color.BLUE);
		top.setBackground(Color.CYAN);
		left.setBackground(Color.GREEN);
		right.setBackground(Color.YELLOW);
		center.setBackground(Color.red);
		
		//top panel settings
		this.setTopPanel();
		this.setRightPanel();
		this.setBottomPanel();
		this.setLeftPanel();
		this.setCenterPanel();
		
		
		add(top, layout.NORTH);
		add(bottom,layout.SOUTH);
		add(left,layout.WEST);
		add(right,layout.EAST);
		add(center,layout.CENTER);
		
		
		setVisible(true);
	}
	
	private void setBottomPanel()
	{
		bottom.add(makeTextPanel("Thesaurus summary"));
	}
	private void setTopPanel()
	{
		
		langList=new JComboBox(lang);
		top.add(new JLabel("Select Language"));
		top.add(langList);
		
		searchbutton=new JButton("Search");
		searchfield=new JTextField(25);
		
		top.add(searchfield);
		top.add(searchbutton);
		
	}
	private void setCenterPanel()
	{
		
	}
	private void setRightPanel()
	{
		right.add(makeTextPanel("History panel"));
		
	}
	private void setLeftPanel()
	{
		leftpane=new JTabbedPane(JTabbedPane.LEFT);
		
		pane1= makeTextPanel("ListView");
		pane2= makeTextPanel("TreeView");
		
		leftpane.addTab("ListView", null, pane1,
                "Does nothing");
        leftpane.setMnemonicAt(0, KeyEvent.VK_1);
		
        leftpane.addTab("TreeView", null, pane2,
                "Does nothing");
        leftpane.setMnemonicAt(1, KeyEvent.VK_1);
        
        leftpane.setTabLayoutPolicy(JTabbedPane.WRAP_TAB_LAYOUT);
        left.setMinimumSize(new Dimension(100, getHeight()));
        
        left.add(leftpane);
	}
	
	 protected JComponent makeTextPanel(String text) {
	        JPanel panel = new JPanel(false);
	        JLabel filler = new JLabel(text);
	        filler.setHorizontalAlignment(JLabel.CENTER);
	        panel.setLayout(new GridLayout(1, 1));
	        panel.add(filler);
	        return panel;
	    }

}
