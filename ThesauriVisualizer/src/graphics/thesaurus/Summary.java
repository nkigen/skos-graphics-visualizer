package graphics.thesaurus;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

import load.*;
public class Summary extends JInternalFrame {

	
	public Summary()
	{
		super("Thesauri Summary");
		setSize(400, 350);
		displayGraphics();
	}
	private void displayGraphics()
	{
		setClosable(true);
		setIconifiable(true); 
		setBackground(Color.white);
		setMaximizable(false);
		FlowLayout layout = new FlowLayout();
		
		layout.setAlignOnBaseline(true);
		setLayout(layout);
		
		add(new JLabel("<html><b>Summary of </b> </html>"));
		
		
		setVisible(true);
	}


}
