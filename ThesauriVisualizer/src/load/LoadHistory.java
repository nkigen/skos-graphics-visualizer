package load;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class LoadHistory {
	
	private ArrayList<String> history;
	
	
	public LoadHistory(String filename)
	{
			history=new ArrayList<String>();
			
			try
			{
			BufferedReader file= new BufferedReader(new FileReader(filename));
			String line;
			while((line=file.readLine())!=null)
			{
				history.add(line);
				
			}
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
	
	}
	
	
	public ArrayList<String> getHistory()
	{
		return history;
	}

}
