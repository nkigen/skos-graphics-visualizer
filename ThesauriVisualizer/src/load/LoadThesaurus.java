package load;

import graphics.thesaurus.Concept;
import graphics.thesaurus.Search;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.semanticweb.skosapibinding.SKOSManager;
import org.semanticweb.skos.*;

import uk.ac.manchester.cs.skos.SKOSEntityAssertionImpl;
public class LoadThesaurus {

	private SKOSManager manager;
	private SKOSDataset dataset;
	private Map<URI, Concept> thesauri;
	private String uri;
	public LoadThesaurus (String uri)
	{
		thesauri= new HashMap<URI,Concept>();
		this.uri=uri;
		
	}
	
	
	public void load()
	{
		try
		{
		manager= new SKOSManager();
		
		System.out.println("Load thesaurus");
		
		dataset= manager.loadDataset(URI.create(uri));
		
		for(SKOSConcept concept: dataset.getSKOSConcepts())
		{
			
			
			Concept con=new Concept(concept.getURI());
			
			
			thesauri.put(concept.getURI(), con);
		System.out.println("concept :" + concept.getURI());
			
		/*	for(SKOSEntity broader : concept.getSKOSRelatedEntitiesByProperty(dataset,manager.getSKOSDataFactory().getSKOSBroaderProperty()))
			{
				con.setBroader(broader);
			  
			 
				
			}
			
			*/
			for(SKOSAnnotation annotation : concept.getSKOSAnnotations(dataset))
			{
				System.out.println("Annotation: " + annotation.getURI().getFragment());
				
				
				
				if(annotation.isAnnotationByConstant())
				{
					if (annotation.getAnnotationValueAsConstant().isTyped()) {
                        SKOSTypedLiteral tl = annotation.getAnnotationValueAsConstant().getAsSKOSTypedLiteral();
                        System.out.print("IS TYPED: " + tl.getLiteral() + " Type: " + tl.getDataType().getURI());
                    }
                    else {
                    	String literal=null;
                    	String lang=null;
                        SKOSUntypedLiteral tl = annotation.getAnnotationValueAsConstant().getAsSKOSUntypedLiteral();
                        System.out.print("untyped literal :" + tl.getLiteral());
                        literal=tl.getLiteral();
                        if (tl.hasLang()) {
                            System.out.print("@" + tl.getLang());
                            lang=tl.getLang();
                        }
                        if(annotation.getURI().getFragment().trim().equals("prefLabel"))
        				{
        					con.setPrefLabel(literal,lang);
        				}
                        else if(annotation.getURI().getFragment().trim().equals("altLabel"))
                        {
                        	con.setAltLabel(literal, lang);
                        }
                        else if(annotation.getURI().getFragment().trim().equals("scopeNote"))
                        {
                        	con.setScopeNote(literal, lang);
                        }
                        else if(annotation.getURI().getFragment().trim().equals("definition"))
                        {
                        	;
                       	}
                        else if(annotation.getURI().getFragment().trim().equals("prefSymbol"))
                        {
                        	;
                       	}
                        else if(annotation.getURI().getFragment().trim().equals("notation"))
                        {
                        	;
                       	}
                        else if(annotation.getURI().getFragment().trim().equals("altSymbol"))
                        {
                        	;
                       	}
                        else if(annotation.getURI().getFragment().trim().equals("example"))
                        {
                        	;
                       	}
                        
                    }
                    System.out.println("");
                }
                else {
                    System.out.println("get anno value :"+annotation.getAnnotationValue().getURI().toString());
                    
                    switch(annotation.getURI().getFragment().trim())
                    {
                    case "narrower" : con.setNarrower(annotation.getAnnotationValue().getURI());
                    case "broader" : con.setBroader(annotation.getAnnotationValue().getURI());
                    case "inScheme" : con.setinScheme(annotation.getAnnotationValue().getURI());
                    case "related" : con.setRelated(annotation.getAnnotationValue().getURI());
                    default: ;
                    	
                    }
                }
				}
			
			/*for(SKOSEntity narrower : concept.getSKOSRelatedEntitiesByProperty(dataset,manager.getSKOSDataFactory().getSKOSNarrowerProperty()))
			{
				con.setNarrower(narrower);
			}
			*/
			
		}
		
		
		}
		catch(SKOSCreationException e)
		{
			e.printStackTrace();
		}
	}
	
	public SKOSManager getManager()
	{
		return manager;
	}
	public SKOSDataset getDataset()
	{
		return dataset;
	}
	
	public Map<URI,Concept> getVocabulary()
	{
		return thesauri;
	}
	
	public static int getResponseCode(String urlString) throws MalformedURLException, IOException {
	    URL u = new URL(urlString); 
	    HttpURLConnection huc =  (HttpURLConnection)  u.openConnection(); 
	    
	    huc.setRequestMethod("GET"); 
	    huc.connect(); 
	    return huc.getResponseCode();
	}

	public void test()
	{
		Iterator<Entry<URI,Concept>> it= thesauri.entrySet().iterator();
		  while (it.hasNext())
		  {
			  
			  Entry pair=it.next();
			  
			  Concept concept=(Concept) pair.getValue();
			  
			  Iterator<Entry<String,String>> pref= concept.getPrefLabel().entrySet().iterator();
			  
			  while(pref.hasNext())
			  {
				  Entry prefpair=pref.next();

				  System.out.println("pref label :" + prefpair.getKey() + "@" + prefpair.getValue());
				  
			  }
			  
		  }
	}
	
	
	/*public static void main(String[] args)
	{
		LoadThesaurus l=new LoadThesaurus("http://localhost/Agrovoc/AGROVOC.skos.xml");
		l.load();
		//l.test();
		new Search("Neutralization testsnb", l.getVocabulary());
		 
		
	}*/
}
