package load;

import java.awt.ComponentOrientation;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class FileChooser extends JFileChooser {
	
	
	public FileChooser()
	{
		super();
		addChoosableFileFilter(new FileNameExtensionFilter(".rdf", ".owl"));
		applyComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
	}
	

    @Override public File getSelectedFile()
    {
		return null;
      
    }

}
